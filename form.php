﻿<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <title>HTTP authorization[5]</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href = "style.css">
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    
    <style>
      body{
        font-family: cursive
      }
      form {
        border: double;
        padding: 4px;

      }
      p{
        display: flex;

      }
      select {
        align-items: center;
        justify-content: center;
      }
      .button {

        text-decoration: none;
        background-color: white;
        color: black;
        padding: 2px 6px 2px 6px;
        border-top: 1px solid black;
        border-right: 1px solid black;
        border-bottom: 1px solid black;
        border-left: 1px solid black;
      }

      /* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
      .error {
      border: 2px solid red;
      }
    </style>
  </head>
  <body>
    
    <div class="center row">
      <div class="col-4"></div>
      <form action="" method="POST">
        <div class="form-group">
        <p>Как вас зовут?</p>
        <input size='40' type='text' maxlength='25' name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>"> <br>
        </div>
        <div class="form-group">
        <p>Введите ваш email:</p>
        <input size="40" name="email" type="text" maxlength="60" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"><br>
		</div>
		<div class="form-group">
        <p>Выберите год рождения:</p>
        <select name="year">
          <?php for ($i = 1900; $i < 2020; $i++){ ?>
          <option value="<?php print $i; ?>" <?= $i == $values['year'] ? 'selected' : ""?>><?= $i; ?></option>
          <?php  } ?>
        </select><br>
        </div>
        <div class="form-group">
            <div <?php if ($errors['sex']) {print 'class="error"';} ?> value="<?php print $values['sex']; ?>"><p>Выберите пол:</p>
              <label class="radio">
                <input type="radio" name="sex" value='0' checked <?php if(isset($values['sex']) && $values['sex'] == '0') { echo 'checked="checked"';}?> />
                Мужской
              </label>
              <label class="radio">
                <input type="radio" name="sex" value='1' <?php if(isset($values['sex']) && $values['sex'] == '1') { echo 'checked="checked"';}?>/>
                Женский
              </label>
            </div>
		</div>
		<div class="form-group">
            <div <?php if ($errors['abilities']) {print 'class="error"';} ?> value="<?php print $values['abilities']; ?>">
              <p>Какие у вас сверхспособности?</p>
              <select multiple name="abilities[]">
                <option name="abilities" value="god">бессмертие</option>
                <option name="abilities" value="idclip">прохождение сквозь стены</option>
                <option name="abilities" value="levitation">левитация</option>
              </select><br>
            </div>
        </div>
        <div class="form-group">
            <p>Ваша биография?</p>
            <textarea rows="5" cols="60" name="bio"  <?php if($errors['bio']) {
            print 'class="error"';}?>><?php print $values['bio'];?></textarea>
        </div>
        <div class="form-group">
        <input type="checkbox" name="accept" value='1' <?php if ($errors['accept']) {print 'class="error"';} ?> value="<?php print $values['accept']; ?>">
		 Принимаю согласие
        </div>
        <input type="submit" value="Занести данные" class="button" />
        
        <a href="logout.php" class="button"> Выйти из аккаунта </a>


        <?php
          if (!empty($messages)) {
            print('<div id="messages">');
                // Выводим все сообщения.
                foreach ($messages as $message) {
                  print($message);
                }
            print('</div>');
          }
        ?>
      </form>
    </div>
  </body>
</html>